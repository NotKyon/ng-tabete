/*
 * 
 * Control panel for various things.
 * It feeds.
 * 
 */

#include <gtk/gtk.h>

#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "getexename.h"

#ifndef PATH_MAX
# if defined( _PATH_MAX )
#  define PATH_MAX _PATH_MAX
# else
#  define PATH_MAX 4096
# endif
#endif

#define KILOBYTES(Kilobytes_) ((Kilobytes_)*1024)
#define MEGABYTES(Megabytes_) ((Megabytes_)*KILOBYTES(1024))

static const char *
va( const char *fmt, ... ) {
	static char buf[ KILOBYTES(128) ];
	static size_t off = 0;

	va_list args;
	int num_written;
	const char *p;

	buf[ off ] = '\0';

	va_start( args, fmt );
	num_written = vsnprintf( &buf[ off ], sizeof(buf) - off, fmt, args );
	va_end( args );

	p = &buf[ off ];

	if( num_written < 0 ) {
		num_written = 0;
	}

	off += (size_t)(num_written);

	if( off + KILOBYTES(4) > sizeof(buf) ) {
		off = 0;
	}

	return p;
}

static const char *
getexedir( char *dst, size_t dstn ) {
	char *p;

	if( !getexename( dst, dstn ) ) {
		return ( const char * )0;
	}

	if( ( p = strrchr( dst, '/' ) ) != ( char * )0 ) {
		*( p + 1 ) = '\0';
	}

	return dst;
}

static void
invoke_script( const char *scriptname ) {
	const char *statusname;
	int exit_status;

	assert( scriptname != NULL &&
		"scriptname is NULL" );

	printf( "Executing `%s`...\n", scriptname );
	fflush( NULL );

	// Auto-insert "./" in front of script name in case missing,
	// but only if locality not already present
	if( strchr( scriptname, '/' ) == ( const char * )0 ) {
		scriptname = va( "./%s", scriptname );
	}

	statusname = "OK";
	if( ( exit_status = system( scriptname ) ) != EXIT_SUCCESS ) {
		statusname = "KO";

		fprintf( stderr, "`%s` failed with exit code %i (0x%.8X)\n",
			scriptname,
			exit_status,
			exit_status
		);
	}

	printf( "Finished `%s` [%s]\n",
		scriptname,
		statusname
	);

	fflush( NULL );
}

static void
rcperf_slide( unsigned value /* range is 0...1024 (inclusive) */ ) {
	invoke_script( va( "tabete_perf.sh %u", value ) );
}

static void
rcperf_value_changed( GtkRange *range, gpointer user_data ) {
	gdouble valf;
	unsigned valu;

	((void)user_data);

	valf = gtk_range_get_value( range );
	valu = (unsigned)(valf);

	rcperf_slide( valu );
}

static void
network_on( void ) {
	invoke_script( "tabete_ntwk.sh 1" );
}
static void
network_off( void ) {
	invoke_script( "tabete_ntwk.sh 0" );
}

static void
network_toggled( GtkToggleButton *ntwkbtn, gpointer user_data ) {
	((void)user_data);
	
	if( gtk_toggle_button_get_active( ntwkbtn ) ) {
		network_on();
	} else {
		network_off();
	}
}

static void
tunnel_on( void ) {
	invoke_script( "tabete_tnnl.sh 1" );
}
static void
tunnel_off( void ) {
	invoke_script( "tabete_tnnl.sh 0" );
}

static void
tunnel_toggled( GtkToggleButton *tnnlbtn, gpointer user_data ) {
	((void)user_data);
	
	if( gtk_toggle_button_get_active( tnnlbtn ) ) {
		tunnel_on();
	} else {
		tunnel_off();
	}
}

static void
mountdisk_activate( void ) {
	invoke_script( "tabete_dsk+.sh" );
}
static void
unmountdisk_activate( void ) {
	invoke_script( "tabete_dsk-.sh" );
}

static void
mounttunnel_activate( void ) {
	invoke_script( "tabete_tnl+.sh" );
}
static void
unmounttunnel_activate( void ) {
	invoke_script( "tabete_tnl-.sh" );
}

static void
mountdisk_clicked( GtkWidget *btn, gpointer user_data ) {
	((void)btn);
	((void)user_data);
	
	mountdisk_activate();
}
static void
unmountdisk_clicked( GtkWidget *btn, gpointer user_data ) {
	((void)btn);
	((void)user_data);

	unmountdisk_activate();
}

static void
mounttunnel_clicked( GtkWidget *btn, gpointer user_data ) {
	((void)btn);
	((void)user_data);
	
	mounttunnel_activate();
}
static void
unmounttunnel_clicked( GtkWidget *btn, gpointer user_data ) {
	((void)btn);
	((void)user_data);

	unmounttunnel_activate();
}

static void
killswitch_activate( void ) {
	invoke_script( "tabete_kill.sh" );
}
static void
killswitch_clicked( GtkWidget *killbtn, gpointer user_data ) {
	((void)killbtn);
	((void)user_data);

	killswitch_activate();
}

static void
safestrcat( char *dst, size_t dstn, const char *src ) {
	size_t off;
	char *p;

	p = strchr( dst, '\0' );
	off = (size_t)( p - dst );

	strncpy( p, src, dstn - off );
}
static void
strcatpath( char *dst, size_t dstn, const char *src ) {
	char *p;
	int needsdirsep;

	assert( dst != NULL && "`dst` cannot be NULL" );
	assert( src != NULL && "`src` cannot be NULL" );
	assert( dstn > 1 && "`dstn` must be a reasonable size" );

	needsdirsep = !( p = strrchr( dst, '/' ) ) || p[1]!='\0';
	if( needsdirsep ) {
		safestrcat( dst, dstn, "/" );
	}

	if( *src == '/' ) {
		++src;
	}

	safestrcat( dst, dstn, src );
}

static const char *
get_base_dir( void ) {
	static char buf[ PATH_MAX ] = { '\0' };

	if( buf[0] == '\0' ) {
		const char *p;
		
		if( ( p = getexedir( buf, sizeof( buf ) ) ) != ( const char * )0 ) {
			// good!
			((void)0);
		} else {
			if( ( p = getenv( "HOME" ) ) != NULL ) {
				strncpy( buf, p, sizeof( buf ) - 1 );
			} else if( !getcwd( buf, sizeof( buf ) ) ) {
				strncpy( buf, "/usr/", sizeof( buf ) - 1 );
			}
			buf[ sizeof( buf ) - 1 ] = '\0';

			strcatpath( buf, sizeof( buf ), ".etc/tabete/" );
	}
	}

	return buf;
}
static const char *
get_title_filename( void ) {
	static char buf[ PATH_MAX ] = { '\0' };

	if( buf[0] == '\0' ) {
		strncpy( buf, get_base_dir(), sizeof( buf ) );
		safestrcat( buf, sizeof( buf ), "window-title.txt" );
	}

	return buf;
}

static const char *
read_file_line( char *dst, size_t dstn, const char *filename ) {
	FILE *fp;
	char *p;

	assert( dst != NULL && "`dst` cannot be NULL" );
	assert( filename != NULL && "`filename` cannot be NULL" );
	assert( (int)(dstn) > 0 );

	fp = fopen( filename, "r" );
	if( !fp ) {
		int e;

		e = errno;

		fprintf( stderr, "%s: failed to open: %s [%i]\n",
			filename,
			strerror( e ),
			e
		);

		return NULL;
	}

	if( !fgets( dst, (int)(dstn), fp ) ) {
		fclose( fp );
		fp = NULL;

		fprintf( stderr, "%s: failed to read line\n", filename );

		return NULL;
	}

	fclose( fp );
	fp = NULL;

	if( ( p = strchr( dst, '\n' ) ) != NULL ) {
		*p = '\0';
	}

	return dst;
}

static const char *
conf_get_window_title( void ) {
	static char buf[ 1024 ] = { '\0' };

	if( buf[0] == '\0' ) {
		if( !read_file_line( buf, sizeof( buf ), get_title_filename() ) ) {
			strncpy( buf, "Tabete", sizeof( buf ) - 1 );
			buf[ sizeof( buf ) - 1 ] = '\0';
		}
	}

	return buf;
}

static void
activate
(
	GtkApplication* app,
	gpointer        user_data
)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *perfslider;
	GtkWidget *nettoggle;
	GtkWidget *tunneltoggle;
	GtkWidget *killswitch;
	GtkWidget *diskhbox;
	GtkWidget *tunnelhbox;
	GtkWidget *disk_mount, *disk_unmount;
	GtkWidget *tunnel_mount, *tunnel_unmount;

	((void)user_data);

	window = gtk_application_window_new( app );

	gtk_window_set_title( GTK_WINDOW(window), conf_get_window_title() );
	gtk_window_set_default_size( GTK_WINDOW(window), 320, 280 );

	gtk_container_set_border_width( GTK_CONTAINER(window), 5 );
	
	vbox = gtk_box_new( GTK_ORIENTATION_VERTICAL, 1 );
	gtk_container_add( GTK_CONTAINER(window), vbox );

	perfslider =
		gtk_scale_new_with_range( GTK_ORIENTATION_HORIZONTAL, 0, 1024, 1 );
	nettoggle = gtk_check_button_new_with_label( "Network Toggle" );
	tunneltoggle = gtk_check_button_new_with_label( "Tunnel Toggle" );
	killswitch = gtk_button_new_with_label( "Killswitch" );

	diskhbox = gtk_box_new( GTK_ORIENTATION_HORIZONTAL, 1 );
	tunnelhbox = gtk_box_new( GTK_ORIENTATION_HORIZONTAL, 1 );

	disk_mount = gtk_button_new_with_label( "Mount disk" );
	disk_unmount = gtk_button_new_with_label( "Unmount disk" );
	
	gtk_box_pack_start( GTK_BOX(diskhbox), disk_mount, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(diskhbox), disk_unmount, TRUE, TRUE, 0 );
	
	tunnel_mount = gtk_button_new_with_label( "Mount tunnel" );
	tunnel_unmount = gtk_button_new_with_label( "Unmount tunnel" );
	
	gtk_box_pack_start( GTK_BOX(tunnelhbox), tunnel_mount, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(tunnelhbox), tunnel_unmount, TRUE, TRUE, 0 );
	
	gtk_box_pack_start( GTK_BOX(vbox), perfslider, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(vbox), nettoggle, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(vbox), tunneltoggle, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(vbox), diskhbox, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(vbox), tunnelhbox, TRUE, TRUE, 0 );
	gtk_box_pack_start( GTK_BOX(vbox), killswitch, TRUE, TRUE, 0 );
	
	g_signal_connect( G_OBJECT(perfslider), "value-changed",
		G_CALLBACK(rcperf_value_changed), NULL );
	g_signal_connect( G_OBJECT(nettoggle), "toggled",
		G_CALLBACK(network_toggled), NULL );
	g_signal_connect( G_OBJECT(tunneltoggle), "toggled",
		G_CALLBACK(tunnel_toggled), NULL );
	g_signal_connect( G_OBJECT(killswitch), "clicked",
		G_CALLBACK(killswitch_clicked), NULL );

	g_signal_connect( G_OBJECT(disk_mount), "clicked",
		G_CALLBACK(mountdisk_clicked), NULL );
	g_signal_connect( G_OBJECT(disk_unmount), "clicked",
		G_CALLBACK(unmountdisk_clicked), NULL );
	g_signal_connect( G_OBJECT(tunnel_mount), "clicked",
		G_CALLBACK(mounttunnel_clicked), NULL );
	g_signal_connect( G_OBJECT(tunnel_unmount), "clicked",
		G_CALLBACK(unmounttunnel_clicked), NULL );

	gtk_widget_show_all( window );
}

int
main
(
	int    argc,
	char **argv
)
{
	GtkApplication *app;
	int status;

	printf( "base_dir: `%s`\n", get_base_dir() );
	printf( "title_file: `%s`\n", get_title_filename() );
	printf( "\n" );

	app = gtk_application_new( "loli.tabete", G_APPLICATION_FLAGS_NONE );
	g_signal_connect( app, "activate", G_CALLBACK(activate), NULL );

	status = g_application_run( G_APPLICATION(app), argc, argv );

	g_object_unref( app );

	return status;
}
