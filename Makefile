CC ?= gcc

CFLAGS ?= -std=gnu11 -W -Wall -pedantic -Warray-bounds -fno-strict-aliasing

.PHONY: all clean run debug

all: target/tabete

clean:
	-@rm -rf target/tabete

run: target/tabete
	"./$<"

debug: target/tabete
	gdb --args "$<"

target/tabete: tabete-main.c getexename.c
	@mkdir -p "$(dir $@)"
	$(CC) `pkg-config --cflags gtk+-3.0` $(CFLAGS) -o "$@" $+ `pkg-config --libs gtk+-3.0`
