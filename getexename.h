#ifndef GETEXENAME_H
#define GETEXENAME_H

#include <stddef.h> /* size_t */

extern char *getexename(char *buff, size_t n);

#endif
